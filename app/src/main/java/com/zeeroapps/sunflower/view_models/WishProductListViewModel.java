package com.zeeroapps.sunflower.view_models;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;

import com.zeeroapps.sunflower.data.ProductAndWishProducts;
import com.zeeroapps.sunflower.data.WishProduct;
import com.zeeroapps.sunflower.data.WishProductRepository;

import java.util.ArrayList;
import java.util.List;

public class WishProductListViewModel extends ViewModel {

    public  LiveData<List<ProductAndWishProducts>> productAndwishproducts;
    public  LiveData<List<WishProduct>> wishproducts;
    public WishProductRepository wishProductRepository;

    public WishProductListViewModel(WishProductRepository wishProductRepository) {
        wishproducts = wishProductRepository.getwishproducts();
        this.wishProductRepository = wishProductRepository;
        productAndwishproducts =
                Transformations.map(wishProductRepository.getproductAndwishproducts(), products -> {
                    List<ProductAndWishProducts> productsListNew = new ArrayList<>();
                    for (int i = 0; i < products.size(); i++) {
                        //Log.d("Loop","product: "+products.get(i).getproduct().getImageUrl());
                        if (products.get(i).getWishProducts() != null && !products.get(i).getWishProducts().isEmpty()) {
                            productsListNew.add(products.get(i));
                        }
                    }
                    return productsListNew;
                });
    }

    public LiveData<List<WishProduct>> getwishproducts() {
        return wishproducts;
    }

    public LiveData<List<ProductAndWishProducts>> getproductAndwishproducts() {
        return productAndwishproducts;
    }

    public WishProductRepository getWishProductRepository() {
        return wishProductRepository;
    }


}
