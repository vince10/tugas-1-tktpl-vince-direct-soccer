package com.zeeroapps.sunflower.view_models;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.zeeroapps.sunflower.data.Product;
import com.zeeroapps.sunflower.data.ProductAndWishProducts;
import com.zeeroapps.sunflower.data.ShoppingCartProduct;
import com.zeeroapps.sunflower.data.ShoppingCartProductRepository;
import com.zeeroapps.sunflower.data.WishProduct;
import com.zeeroapps.sunflower.data.WishProductRepository;
import com.zeeroapps.sunflower.data.ProductRepository;

import java.util.ArrayList;
import java.util.List;

public class ProductDetailViewModel extends ViewModel {
    public  LiveData<List<ProductAndWishProducts>> productAndwishproducts;
    public  LiveData<List<WishProduct>> wishproducts;
    public WishProductRepository wishProductRepository;
    public ShoppingCartProductRepository shoppingCartProductRepository;
    private final LiveData<WishProduct> wishproductForproduct;
    LiveData<Boolean> isproducted;
    public LiveData<Product> product;
    String productId;

    public ProductDetailViewModel(ProductRepository productRepository, ShoppingCartProductRepository shoppingCartProductRepository,
                                  WishProductRepository wishProductRepository, String productId){
        Log.d("PDVM :","Init PDVM");
        this.productId = productId;

        this.product = productRepository.getproduct(productId);
        this.wishProductRepository = wishProductRepository;
        this.shoppingCartProductRepository = shoppingCartProductRepository;
        wishproductForproduct = wishProductRepository.getwishproductForproduct(productId);


        Log.d("PDVM :",Boolean.toString(wishProductRepository == null));





    }

    public void addproductToWishProduct() {

        WishProduct wishProduct = new WishProduct(productId);
        wishProductRepository.createwishproduct(wishProduct);


        //wishproductRepository.createwishproduct(productId);
    }

    public void addProductToShoppingCart(){
        ShoppingCartProduct shoppingCartProduct = new ShoppingCartProduct(productId);
        shoppingCartProductRepository.createShoppingCartProduct(shoppingCartProduct);

    }


    public void deleteWishlistItem(){
        List<ProductAndWishProducts> productingsListNew = new ArrayList<>();
        Log.d("PDVW dWI",""+"cmon");
        Log.d("PDWI dWI",product.toString()+"sss");
        Log.d("PDVW dWI",productId);






        //wishproductRepository.getwishproductForproduct(productId).observe(this);





    }





    public LiveData<Product> getproduct(){
        return product;
    }

//    @Override
//    protected void onCleared() {
//        super.onCleared();
//        viewModelScope.cancel();
//    }
}
