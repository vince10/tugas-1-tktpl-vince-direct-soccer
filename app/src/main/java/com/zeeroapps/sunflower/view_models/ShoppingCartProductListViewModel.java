package com.zeeroapps.sunflower.view_models;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;

import com.zeeroapps.sunflower.data.ProductAndShoppingCartProducts;
import com.zeeroapps.sunflower.data.ShoppingCartProduct;
import com.zeeroapps.sunflower.data.ShoppingCartProductRepository;

import java.util.ArrayList;
import java.util.List;

public class ShoppingCartProductListViewModel extends ViewModel {

    public  LiveData<List<ProductAndShoppingCartProducts>> productAndShoppingCartProducts;
    public  LiveData<List<ShoppingCartProduct>> ShoppingCartProducts;
    public ShoppingCartProductRepository ShoppingCartProductRepository;

    public ShoppingCartProductListViewModel(ShoppingCartProductRepository ShoppingCartProductRepository) {
        ShoppingCartProducts = ShoppingCartProductRepository.getShoppingCartProducts();
        this.ShoppingCartProductRepository = ShoppingCartProductRepository;
        productAndShoppingCartProducts =
                Transformations.map(ShoppingCartProductRepository.getproductAndShoppingCartProducts(), products -> {
                    List<ProductAndShoppingCartProducts> productsListNew = new ArrayList<>();
                    for (int i = 0; i < products.size(); i++) {
                        //Log.d("Loop","product: "+products.get(i).getproduct().getImageUrl());
                        if (products.get(i).getShoppingCartProducts() != null && !products.get(i).getShoppingCartProducts().isEmpty()) {
                            productsListNew.add(products.get(i));
                        }
                    }
                    return productsListNew;
                });
    }

    public LiveData<List<ShoppingCartProduct>> getShoppingCartProducts() {
        return ShoppingCartProducts;
    }

    public LiveData<List<ProductAndShoppingCartProducts>> getproductAndShoppingCartProducts() {
        return productAndShoppingCartProducts;
    }

    public ShoppingCartProductRepository getShoppingCartProductRepository() {
        return ShoppingCartProductRepository;
    }


}
