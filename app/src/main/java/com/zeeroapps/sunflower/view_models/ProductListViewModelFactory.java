package com.zeeroapps.sunflower.view_models;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.annotation.NonNull;

import com.zeeroapps.sunflower.data.ProductRepository;

public class ProductListViewModelFactory extends ViewModelProvider.NewInstanceFactory {
    private ProductRepository productRepository;

    public ProductListViewModelFactory(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        ProductListViewModel productListViewModel = new ProductListViewModel(productRepository);
        return (T) productListViewModel;
    }
}
