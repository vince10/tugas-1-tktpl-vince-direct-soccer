package com.zeeroapps.sunflower.view_models;

import androidx.lifecycle.ViewModel;
import android.content.Context;
import androidx.databinding.ObservableField;

import com.zeeroapps.sunflower.data.ShoppingCartProduct;
import com.zeeroapps.sunflower.data.Product;
import com.zeeroapps.sunflower.data.ProductAndShoppingCartProducts;

import java.text.SimpleDateFormat;
import java.util.Locale;

public class ProductAndShoppingCartProductsViewModel extends ViewModel {

    private final ObservableField<String> imageUrl;
    private final ObservableField<String> productPrice;
    private final ObservableField<String> productName;
    private final ObservableField<String> numOfproducts;
    private Product product;
    private ShoppingCartProduct ShoppingCartProduct;
    private int numberOfproducts;

    public ProductAndShoppingCartProductsViewModel(Context context, ProductAndShoppingCartProducts productings) {
        this.product = productings.getProduct();
        this.numberOfproducts = productings.getShoppingCartProducts().size();
        this.ShoppingCartProduct = productings.getShoppingCartProducts().get(0);

        SimpleDateFormat dateFormat = new SimpleDateFormat("MMM d, yyyy", Locale.US);

        String numberOfproductsStr = Integer.toString(numberOfproducts);



        imageUrl = new ObservableField<String>(product.getImageUrl());

        numOfproducts = new ObservableField<String>("Number of products producted:"+numberOfproductsStr);
        productName = new ObservableField<>(product.getName());
        productPrice = new ObservableField<>(product.getPriceInString());
        //Todo: fix waterDate field
    }

    public ObservableField<String> getProductName() {
        return productName;
    }

    public ObservableField<String> getProductPrice() {
        return productPrice;
    }


    public ObservableField<String> getImageUrl() {
        return imageUrl;
    }



    public ObservableField<String> getNumOfproducts() {
        return numOfproducts;
    }

    public int getNumberOfproducts() {
        return numberOfproducts;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public ShoppingCartProduct getShoppingCartProduct() {
        return ShoppingCartProduct;
    }

    public void setShoppingCartProduct(ShoppingCartProduct ShoppingCartProduct) {
        this.ShoppingCartProduct = ShoppingCartProduct;
    }
}
