package com.zeeroapps.sunflower.view_models;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.annotation.NonNull;

import com.zeeroapps.sunflower.data.WishProductRepository;

public class WishProductListViewModelFactory extends ViewModelProvider.NewInstanceFactory {
    WishProductRepository wishProductRepository;

    public WishProductListViewModelFactory(WishProductRepository repository) {
        this.wishProductRepository = repository;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new WishProductListViewModel(wishProductRepository);
    }
}
