package com.zeeroapps.sunflower.view_models;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;

import com.zeeroapps.sunflower.data.Product;
import com.zeeroapps.sunflower.data.ProductRepository;

import java.util.List;

public class ProductListViewModel extends ViewModel {
    private ProductRepository productRepository;
    private int NO_GROW_ZONE = -1;
    private MutableLiveData<Integer> growZoneNumber = new MutableLiveData<>();
    private MediatorLiveData<List<Product>> productList = new MediatorLiveData<>();

    ProductListViewModel(ProductRepository productRepository) {
        this.productRepository = productRepository;
        growZoneNumber.setValue(NO_GROW_ZONE);

        LiveData<List<Product>> liveproductList = Transformations.switchMap(growZoneNumber, (gz_no) -> {
            if (gz_no == NO_GROW_ZONE) {
                return productRepository.getproducts();
            } else {
                return productRepository.getproductsByGrowZoneNumber(gz_no);
            }
        });

        productList.addSource(liveproductList, products -> productList.setValue(products));
    }


    public MediatorLiveData<List<Product>> getproducts() {
        return productList;
    }

    public void setGrowZoneNumber(int no) {
        growZoneNumber.setValue(no);
    }

    public void clearGrowZoneNumber() {
        growZoneNumber.setValue(NO_GROW_ZONE);
    }

    public boolean isFiltered() {
        return growZoneNumber.getValue() != NO_GROW_ZONE;
    }

}
