package com.zeeroapps.sunflower.view_models;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.annotation.NonNull;

import com.zeeroapps.sunflower.data.ShoppingCartProductRepository;

public class ShoppingCartProductListViewModelFactory extends ViewModelProvider.NewInstanceFactory {
    ShoppingCartProductRepository ShoppingCartProductRepository;

    public ShoppingCartProductListViewModelFactory(ShoppingCartProductRepository repository) {
        this.ShoppingCartProductRepository = repository;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new ShoppingCartProductListViewModel(ShoppingCartProductRepository);
    }
}
