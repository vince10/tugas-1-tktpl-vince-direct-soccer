package com.zeeroapps.sunflower.view_models;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.annotation.NonNull;

import com.zeeroapps.sunflower.data.ShoppingCartProductRepository;
import com.zeeroapps.sunflower.data.WishProductRepository;
import com.zeeroapps.sunflower.data.ProductRepository;

public class ProductDetailViewModelFactory extends ViewModelProvider.NewInstanceFactory {
    private ProductRepository productRepository;
    private WishProductRepository wishProductRepository;
    private ShoppingCartProductRepository shoppingCartProductRepository;
    private String plantId;

    public ProductDetailViewModelFactory(ProductRepository productRepository,
                                         ShoppingCartProductRepository shoppingCartProductRepository,
                                         WishProductRepository wishProductRepository, String plantId) {
        this.productRepository = productRepository;
        this.wishProductRepository = wishProductRepository;
        this.shoppingCartProductRepository = shoppingCartProductRepository;
        this.plantId = plantId;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new ProductDetailViewModel(productRepository, shoppingCartProductRepository,wishProductRepository, plantId);
    }
}
