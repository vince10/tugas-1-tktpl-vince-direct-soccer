package com.zeeroapps.sunflower.data;

import androidx.lifecycle.LiveData;

import java.util.List;

public class ProductRepository {
    private ProductDao productDao;
    private static volatile ProductRepository instance;

    ProductRepository(ProductDao productDao) {
        this.productDao = productDao;
    }

    public LiveData<List<Product>> getproducts() {
        return productDao.getproducts();
    }

    public LiveData<Product> getproduct(String id) {
        return productDao.getproduct(id);
    }

    public LiveData<List<Product>> getproductsByGrowZoneNumber(int growZoneNo) {
        return productDao.getproductsByGrowZoneNumber(growZoneNo);
    }

    public static ProductRepository getInstance(ProductDao productDao) {
        if (instance == null) {
            synchronized(ProductRepository.class) {
                if (instance == null)
                    instance = new ProductRepository(productDao);
            }
        }
        return instance;
    }
}
