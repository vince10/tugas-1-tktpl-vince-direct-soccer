package com.zeeroapps.sunflower.data;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface ProductDao {
    @Insert(onConflict = REPLACE)
    void insertAll(List<Product> products);

    @Query("SELECT * FROM products ORDER BY name")
    LiveData<List<Product>> getproducts();

    @Query("SELECT * FROM products WHERE id = :productId")
    LiveData<Product> getproduct(String productId);

    @Query("SELECT * FROM products WHERE growZoneNumber = :growZoneNumber ORDER BY name")
    LiveData<List<Product>> getproductsByGrowZoneNumber(int growZoneNumber);
}
