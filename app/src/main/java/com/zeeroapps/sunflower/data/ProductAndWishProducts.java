package com.zeeroapps.sunflower.data;

import androidx.room.Embedded;
import androidx.room.Relation;

import java.util.ArrayList;
import java.util.List;

public class ProductAndWishProducts {
    @Embedded
    Product product;

    @Relation(parentColumn = "id", entityColumn = "product_id")
    List<WishProduct> wishProducts = new ArrayList<>();

    public Product getProduct() {
        return product;
    }

    public List<WishProduct> getWishProducts(){
        return wishProducts;
    }
}
