package com.zeeroapps.sunflower.data;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Transaction;

import java.util.List;

@Dao
public interface ShoppingCartProductDao {

    @Delete
    void deleteShoppingCartProduct(ShoppingCartProduct ShoppingCartProduct);


    @Query("SELECT * FROM shopping_cart_products")
    LiveData<List<ShoppingCartProduct>> getShoppingCartProducts();

    @Query("SELECT * FROM shopping_cart_products")
    List<ShoppingCartProduct> getShoppingCartProductsNonAsync();

    @Query("SELECT * FROM shopping_cart_products WHERE id = :ShoppingCartProductId")
    LiveData<ShoppingCartProduct> getShoppingCartProduct(long ShoppingCartProductId);

    @Query("SELECT * FROM shopping_cart_products WHERE product_id = :productId")
    LiveData<ShoppingCartProduct> getShoppingCartProductForproduct(String productId);

    @Query("SELECT * FROM shopping_cart_products WHERE product_id = :productId")
    ShoppingCartProduct getShoppingCartProductForproductNonAsync(String productId);


    @Transaction
    @Query("SELECT * FROM products")
    LiveData<List<ProductAndShoppingCartProducts>> getproductAndShoppingCartProducts();

    @Transaction
    @Query("SELECT * FROM products")
    List<ProductAndShoppingCartProducts> getproductAndShoppingCartProductsNonAsync();

    @Insert
    long insertShoppingCartProduct(ShoppingCartProduct ShoppingCartProduct);
}
