package com.zeeroapps.sunflower.data;

import androidx.sqlite.db.SupportSQLiteDatabase;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;
import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;

import com.zeeroapps.sunflower.workers.SeedDatabaseWorker;

import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;

@Database(entities = {Product.class, WishProduct.class, ShoppingCartProduct.class}, version = 1, exportSchema = false)
@TypeConverters(Converters.class)
public abstract class AppDatabase extends RoomDatabase {
    public abstract ProductDao productDao();
    public abstract WishProductDao wishProductDao();
    public abstract ShoppingCartProductDao shoppingCartProductDao();
    private static AppDatabase instance = null;

    public static AppDatabase getInstance(Context context) {
        if (instance == null) {
            Log.d("debug","instance == null");
            synchronized (AppDatabase.class) {
                if (instance == null){
                    instance = buildDatabase(context);
                    Log.d("debug","double instance == null");
                }
            }
        }
        Log.d("debug","instance != null");
        return instance;
    }

    private static AppDatabase buildDatabase(Context context) {
        Log.d("Masuk buildDB","True");
        return Room.databaseBuilder(context, AppDatabase.class, "sunflower-db")
                .addCallback(new Callback() {
                    @Override
                    public void onCreate(@NonNull SupportSQLiteDatabase db) {
                        super.onCreate(db);
                        Log.d("Masuk OncreatebuildDB","True");
                        OneTimeWorkRequest workRequest = new OneTimeWorkRequest.Builder(SeedDatabaseWorker.class).build();
                        WorkManager.getInstance().enqueue(workRequest);
                    }
                }).build();
    }

    public static void destroyDatabase() {
        instance = null;
    }
}
