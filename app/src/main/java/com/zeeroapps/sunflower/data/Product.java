package com.zeeroapps.sunflower.data;


import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.databinding.BindingAdapter;
import androidx.annotation.NonNull;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

@Entity(tableName = "products")
public class Product {
    @NonNull
    @PrimaryKey
    @ColumnInfo(name = "id")
    private String productId;
    private String name;
    private String description;
    private int growZoneNumber;
    private int wateringInterval = 7;
    private String imageUrl = "";
    private String sizes;
    private int price;


    @NonNull
    public String getProductId() {
        return productId;
    }

    public void setProductId(@NonNull String productId) {
        this.productId = productId;
    }

    public String getSizes() {
        return sizes;
    }


    public void setSizes(String sizes) {
        this.sizes = sizes;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getPriceInString(){
        return Integer.toString(price);
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getGrowZoneNumber() {
        return growZoneNumber;
    }

    public void setGrowZoneNumber(int growZoneNumber) {
        this.growZoneNumber = growZoneNumber;
    }

    public int getWateringInterval() {
        return wateringInterval;
    }

    public void setWateringInterval(int wateringInterval) {
        this.wateringInterval = wateringInterval;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }


    @BindingAdapter("imageFromUrl")
    public static void imageFromUrl(ImageView view, String imageUrl) {
        if (imageUrl !=  null && !imageUrl.isEmpty())
            Glide.with(view.getContext()).load(imageUrl).into(view);
    }
}
