package com.zeeroapps.sunflower.data;

import androidx.lifecycle.LiveData;
import android.os.AsyncTask;
import android.util.Log;

import java.util.List;

public class WishProductRepository {

    static WishProductRepository instance;
    WishProductDao wishProductDao;

    public WishProductRepository(WishProductDao wishProductDao) {
        this.wishProductDao = wishProductDao;
    }

    public static WishProductRepository getInstance(WishProductDao wishProductDao) {
        if (instance == null) {
            synchronized (WishProduct.class) {
                if (instance == null) {
                    instance = new WishProductRepository(wishProductDao);
                }
            }
        }
        return instance;
    }

    public void createwishproduct(String productId) {
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                WishProduct wishProduct = new WishProduct(productId);
                wishProductDao.insertwishproduct(wishProduct);
                Log.d("createwishproduct",productId);

            }
        });

    }

    public void  createwishproduct( WishProduct wishProduct){
        new CreatewishproductAsyncTask(wishProductDao).execute(wishProduct);
    }

    private static class CreatewishproductAsyncTask extends AsyncTask<WishProduct, Void, Void>{

        private WishProductDao wishProductDao;

        public CreatewishproductAsyncTask(WishProductDao wishProductDao) {
           this.wishProductDao = wishProductDao;
        }


        @Override
        protected Void doInBackground(WishProduct... wishProducts) {
            wishProductDao.insertwishproduct(wishProducts[0]);
            return null;
        }
    }

    public void deletewishproduct(WishProduct wishProduct){
        new DeletewishproductAsyncTask(wishProductDao).execute(wishProduct);
    }

    public void deletewishproduct (String productId){
        WishProduct wishProduct = getwishproductForproduct(productId).getValue();
        new DeletewishproductAsyncTask(wishProductDao).execute(wishProduct);

    }




    private static class DeletewishproductAsyncTask extends AsyncTask<WishProduct,Void,Void>{

        private WishProductDao wishProductDao;

        public DeletewishproductAsyncTask(WishProductDao wishProductDao){
            this.wishProductDao = wishProductDao;
        }

        @Override
        protected Void doInBackground(WishProduct... wishProducts) {
            wishProductDao.deletewishproduct(wishProducts[0]);
            return null;
        }
    }

//    public void deletewishproduct(wishproduct wishproduct){
//        AsyncTask.execute(new Runnable() {
//            @Override
//            public void run() {
//              wishproductDao.deletewishproduct(wishproduct);
//            }
//        });
//    }
//
//    public void deletewishproduct(int wishproductID){
//        AsyncTask.execute(new Runnable() {
//            @Override
//            public void run() {
//               wishproductDao.deletewishproduct(wishproductDao.getwishproduct(wishproductID).getValue());
//            }
//        });
//    }


    public LiveData<WishProduct> getwishproductForproduct(String productId) {
        return wishProductDao.getwishproductForproduct(productId);
    }


    public LiveData<List<WishProduct>> getwishproducts() {
        return wishProductDao.getwishproducts();
    }

    public List<WishProduct> getwishproductsNonAsync() {
        return wishProductDao.getwishproductsNonAsync();
    }

    public LiveData<List<ProductAndWishProducts>> getproductAndwishproducts(){
        return wishProductDao.getproductAndwishproducts();
    }

    public List<ProductAndWishProducts> getproductAndwishproductsNonAsync(){
        return wishProductDao.getproductAndwishproductsNonAsync();
    }



    //    private ExecutorService IO_EXECUTOR = Executors.newSingleThreadExecutor();
//
//    void runOnIoThread() {
//        IO_EXECUTOR.execute();
//    }



}
