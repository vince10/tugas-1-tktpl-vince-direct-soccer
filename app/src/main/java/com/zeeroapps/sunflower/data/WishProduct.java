package com.zeeroapps.sunflower.data;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;
import androidx.room.PrimaryKey;



@Entity(
        tableName = "wish_products",
        foreignKeys = {@ForeignKey(entity = Product.class, parentColumns = {"id"}, childColumns = {"product_id"})},
        indices = {@Index("product_id")}
)
public class WishProduct {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    public long wishProductId = 0;

    @ColumnInfo(name = "product_id")
    public String productId;

    public WishProduct(String productId) {
        this.productId = productId;
    }
}
