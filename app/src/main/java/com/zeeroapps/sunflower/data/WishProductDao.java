package com.zeeroapps.sunflower.data;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Transaction;

import java.util.List;
@Dao
public interface WishProductDao {


    @Delete
    void deletewishproduct(WishProduct wishProduct);


    @Query("SELECT * FROM wish_products")
    LiveData<List<WishProduct>> getwishproducts();

    @Query("SELECT * FROM wish_products")
    List<WishProduct> getwishproductsNonAsync();

    @Query("SELECT * FROM wish_products WHERE id = :wishproductId")
    LiveData<WishProduct> getwishproduct(long wishproductId);

    @Query("SELECT * FROM wish_products WHERE product_id = :productId")
    LiveData<WishProduct> getwishproductForproduct(String productId);

    @Query("SELECT * FROM wish_products WHERE product_id = :productId")
    WishProduct getwishproductForproductNonAsync(String productId);


    @Transaction
    @Query("SELECT * FROM products")
    LiveData<List<ProductAndWishProducts>> getproductAndwishproducts();

    @Transaction
    @Query("SELECT * FROM products")
    List<ProductAndWishProducts> getproductAndwishproductsNonAsync();

    @Insert
    long insertwishproduct(WishProduct wishProduct);

}
