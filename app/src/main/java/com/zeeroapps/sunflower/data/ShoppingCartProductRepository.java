package com.zeeroapps.sunflower.data;

import androidx.lifecycle.LiveData;
import android.os.AsyncTask;
import android.util.Log;

import java.util.List;

public class ShoppingCartProductRepository {

    static ShoppingCartProductRepository instance;
    ShoppingCartProductDao ShoppingCartProductDao;

    public ShoppingCartProductRepository(ShoppingCartProductDao ShoppingCartProductDao) {
        this.ShoppingCartProductDao = ShoppingCartProductDao;
    }

    public static ShoppingCartProductRepository getInstance(ShoppingCartProductDao ShoppingCartProductDao) {
        if (instance == null) {
            synchronized (ShoppingCartProduct.class) {
                if (instance == null) {
                    instance = new ShoppingCartProductRepository(ShoppingCartProductDao);
                }
            }
        }
        return instance;
    }

    public void createShoppingCartProduct(String productId) {
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                ShoppingCartProduct ShoppingCartProduct = new ShoppingCartProduct(productId);
                ShoppingCartProductDao.insertShoppingCartProduct(ShoppingCartProduct);
                Log.d("createShoppingCartProduct",productId);

            }
        });

    }

    public void  createShoppingCartProduct( ShoppingCartProduct ShoppingCartProduct){
        new CreateShoppingCartProductAsyncTask(ShoppingCartProductDao).execute(ShoppingCartProduct);
    }

    private static class CreateShoppingCartProductAsyncTask extends AsyncTask<ShoppingCartProduct, Void, Void>{

        private ShoppingCartProductDao ShoppingCartProductDao;

        public CreateShoppingCartProductAsyncTask(ShoppingCartProductDao ShoppingCartProductDao) {
            this.ShoppingCartProductDao = ShoppingCartProductDao;
        }


        @Override
        protected Void doInBackground(ShoppingCartProduct... ShoppingCartProducts) {
            ShoppingCartProductDao.insertShoppingCartProduct(ShoppingCartProducts[0]);
            return null;
        }
    }

    public void deleteShoppingCartProduct(ShoppingCartProduct ShoppingCartProduct){
        new DeleteShoppingCartProductAsyncTask(ShoppingCartProductDao).execute(ShoppingCartProduct);
    }

    public void deleteShoppingCartProduct (String productId){
        ShoppingCartProduct ShoppingCartProduct = getShoppingCartProductForproduct(productId).getValue();
        new DeleteShoppingCartProductAsyncTask(ShoppingCartProductDao).execute(ShoppingCartProduct);

    }




    private static class DeleteShoppingCartProductAsyncTask extends AsyncTask<ShoppingCartProduct,Void,Void>{

        private ShoppingCartProductDao ShoppingCartProductDao;

        public DeleteShoppingCartProductAsyncTask(ShoppingCartProductDao ShoppingCartProductDao){
            this.ShoppingCartProductDao = ShoppingCartProductDao;
        }

        @Override
        protected Void doInBackground(ShoppingCartProduct... ShoppingCartProducts) {
            ShoppingCartProductDao.deleteShoppingCartProduct(ShoppingCartProducts[0]);
            return null;
        }
    }



    public LiveData<ShoppingCartProduct> getShoppingCartProductForproduct(String productId) {
        return ShoppingCartProductDao.getShoppingCartProductForproduct(productId);
    }


    public LiveData<List<ShoppingCartProduct>> getShoppingCartProducts() {
        return ShoppingCartProductDao.getShoppingCartProducts();
    }

    public List<ShoppingCartProduct> getShoppingCartProductsNonAsync() {
        return ShoppingCartProductDao.getShoppingCartProductsNonAsync();
    }

    public LiveData<List<ProductAndShoppingCartProducts>> getproductAndShoppingCartProducts(){
        return ShoppingCartProductDao.getproductAndShoppingCartProducts();
    }

    public List<ProductAndShoppingCartProducts> getproductAndShoppingCartProductsNonAsync(){
        return ShoppingCartProductDao.getproductAndShoppingCartProductsNonAsync();
    }




}
