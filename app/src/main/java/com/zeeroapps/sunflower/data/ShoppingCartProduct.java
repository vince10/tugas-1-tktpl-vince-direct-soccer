package com.zeeroapps.sunflower.data;


import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;
import androidx.room.PrimaryKey;

@Entity(
        tableName = "shopping_cart_products",
        foreignKeys = {@ForeignKey(entity = Product.class, parentColumns = {"id"}, childColumns = {"product_id"})},
        indices = {@Index("product_id")}
)
public class ShoppingCartProduct {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    public long shoppingCartProductId = 0;

    @ColumnInfo(name = "product_id")
    public String productId;

    public ShoppingCartProduct(String productId) {
        this.productId = productId;
    }
}
