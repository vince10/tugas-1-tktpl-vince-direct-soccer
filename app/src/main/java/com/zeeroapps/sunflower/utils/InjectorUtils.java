package com.zeeroapps.sunflower.utils;

import android.content.Context;

import com.zeeroapps.sunflower.data.AppDatabase;
import com.zeeroapps.sunflower.data.ShoppingCartProductRepository;
import com.zeeroapps.sunflower.data.WishProductRepository;
import com.zeeroapps.sunflower.data.ProductRepository;
import com.zeeroapps.sunflower.view_models.ShoppingCartProductListViewModelFactory;
import com.zeeroapps.sunflower.view_models.WishProductListViewModelFactory;
import com.zeeroapps.sunflower.view_models.ProductDetailViewModelFactory;
import com.zeeroapps.sunflower.view_models.ProductListViewModelFactory;

public class InjectorUtils {


    public static ProductRepository getproductRepository(Context context) {
        return ProductRepository.getInstance(AppDatabase.getInstance(context).productDao());
    }



    public static WishProductRepository getwishproductRepository(Context context) {
        return WishProductRepository.getInstance(AppDatabase.getInstance(context).wishProductDao());
    }

    public  static ShoppingCartProductRepository getShoppingCartProductRepository(Context context){
        return ShoppingCartProductRepository.getInstance(AppDatabase.getInstance(context).shoppingCartProductDao());
    }

    public static ProductListViewModelFactory provideproductListViewModelFactory(Context context) {
        ProductRepository repository = getproductRepository(context);
        ProductListViewModelFactory vmFactory = new ProductListViewModelFactory(repository);
        return vmFactory;
    }

    public static ShoppingCartProductListViewModelFactory provideShoppingCartProductListViewModelFactory(Context context){
        ShoppingCartProductRepository repository = getShoppingCartProductRepository(context);
        return new ShoppingCartProductListViewModelFactory(repository);

    }

    public static WishProductListViewModelFactory providewishproductListViewModelFactory(Context context) {
        WishProductRepository wishProductRepository = getwishproductRepository(context);
        return new WishProductListViewModelFactory(wishProductRepository);
    }

    public static ProductDetailViewModelFactory provideproductDetailViewModelFactory(Context context, String productId) {
        return new ProductDetailViewModelFactory(getproductRepository(context), getShoppingCartProductRepository(context),getwishproductRepository(context), productId);
    }




}
