package com.zeeroapps.sunflower.ui;


import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.zeeroapps.sunflower.R;
import com.zeeroapps.sunflower.adapters.WishProductAdapter;
import com.zeeroapps.sunflower.data.ProductAndWishProducts;
import com.zeeroapps.sunflower.data.WishProduct;
import com.zeeroapps.sunflower.utils.InjectorUtils;
import com.zeeroapps.sunflower.view_models.WishProductListViewModel;
import com.zeeroapps.sunflower.view_models.WishProductListViewModelFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class WishListFragment extends Fragment {

    List<ProductAndWishProducts> productingsList = new ArrayList<>();
    RecyclerView mRecyclerView;
    TextView mTextViewEmptywishlist;

    public WishListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_wishlist, container, false);
        WishProductAdapter adapter = new WishProductAdapter(productingsList);
        adapter.setContext(getContext());


        mTextViewEmptywishlist = view.findViewById(R.id.empty_wishlist);
        mRecyclerView = view.findViewById(R.id.wishlist_list);
        mRecyclerView.setAdapter(adapter);


        Log.d("GF","sebelum subscribeUI");

        subscribeUI(adapter);
        Log.d("GF",adapter.getItemCount()+"");
        return view;
    }

    private void subscribeUI(WishProductAdapter adapter) {
       Log.d("GF","viewModel.wishproducts is null:");
        WishProductListViewModelFactory factory = InjectorUtils.providewishproductListViewModelFactory(getContext());
        WishProductListViewModel viewModel = ViewModelProviders.of(this, factory).get(WishProductListViewModel.class);
        viewModel.wishproducts.observe(getViewLifecycleOwner(), new Observer<List<WishProduct>>() {
            @Override
            public void onChanged(@Nullable List<WishProduct> wishProducts) {
                if (wishProducts != null && !wishProducts.isEmpty()) {
                    mRecyclerView.setVisibility(View.VISIBLE);
                    mTextViewEmptywishlist.setVisibility(View.GONE);
                }else {
                    mRecyclerView.setVisibility(View.GONE);
                    mTextViewEmptywishlist.setVisibility(View.VISIBLE);
                }
            }
        });

        Log.d("GF", "viewModel.wishproducts is null:"+(viewModel.wishproducts == null));
        Log.d("GF", "viewModel.productAndwishproducts is null:"+(viewModel.productAndwishproducts == null));
        Log.d("GF", "viewModel.wishproductRepository is null:"+(viewModel.wishProductRepository == null));
        adapter.setWishProductRepository(viewModel.wishProductRepository);
        Log.d("GF", "adapter.wishproductRepository is null:"+(adapter.getWishProductRepository() == null));








        viewModel.productAndwishproducts.observe(getViewLifecycleOwner(), new Observer<List<ProductAndWishProducts>>() {
            @Override
            public void onChanged(@Nullable List<ProductAndWishProducts> wishproductsList) {
                for (int i = 0;i<wishproductsList.size();i++){
                    Log.d("onChanged GF,i1: ", wishproductsList.get(i).getProduct().getName());
                    Log.d("onChanged GF,i2:", Integer.toString(wishproductsList.get(i).getWishProducts().size()));
                }

                if (wishproductsList != null && !wishproductsList.isEmpty()) {
                    adapter.updateList(wishproductsList);
                }
            }

        });

        Log.d("GF","Size adapter"+adapter.getItemCount()+"");
        Log.d("GF","wooooooooooyyyyyyyyyy");
        Log.d("GF","Size adapter"+adapter.getItemCount()+"");
        Log.d("GF","Size adapter"+adapter.getItemCount()+"");
        Log.d("GF","Size adapter"+adapter.getItemCount()+"");

        Log.d("GF","wooooooooooyyyyyyyyyy");






    }



}
