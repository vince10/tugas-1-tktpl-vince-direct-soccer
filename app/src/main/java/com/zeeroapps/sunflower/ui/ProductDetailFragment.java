package com.zeeroapps.sunflower.ui;


import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.snackbar.Snackbar;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.zeeroapps.sunflower.R;
import com.zeeroapps.sunflower.data.Product;
import com.zeeroapps.sunflower.databinding.FragmentProductDetailsBinding;
import com.zeeroapps.sunflower.utils.InjectorUtils;
import com.zeeroapps.sunflower.view_models.ProductDetailViewModel;
import com.zeeroapps.sunflower.view_models.ProductDetailViewModelFactory;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProductDetailFragment extends Fragment {

    private static final String TAG = "PlantDetailFragment";
    private static final String ARG_ITEM_ITEM = "item_id";

    String shareText;
    ProductDetailViewModel mViewModel;

    public static ProductDetailFragment newInstance(String plantId) {
        Bundle args = new Bundle();
        args.putString(ARG_ITEM_ITEM, plantId);
        ProductDetailFragment fragment = new ProductDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FragmentProductDetailsBinding binding = DataBindingUtil.inflate(inflater,
                R.layout.fragment_product_details, container, false);


        String plantId = ProductDetailFragmentArgs.fromBundle(getArguments()).getProductId();
        String detailType = ProductDetailFragmentArgs.fromBundle(getArguments()).getProductDetailType();
        setupViewModel(plantId);

        binding.setViewModel(mViewModel);
        binding.setLifecycleOwner(this);
//        switch (detailType){
//            case "product":
//                binding.deleteItemFromWishListButton.hide();
//                binding.fab.show();
//                break;
//            case "wishlist":
//                binding.fab.hide();
//                binding.deleteItemFromWishListButton.show();
//                break;
//
//        }
        binding.fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("PDF","masuk on click");
                mViewModel.addproductToWishProduct();

                Snackbar.make(view, R.string.added_product_to_wishlist, Snackbar.LENGTH_LONG).show();
                Log.d("PDF",Integer.toString(binding.fab.getVisibility()));
//                binding.fab.hide();
//                binding.deleteItemFromWishListButton.show();

                //Todo: Hide button after adding plant to WishProduct.
            }
        });

        binding.addToCartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mViewModel.addProductToShoppingCart();
                Snackbar.make(v, R.string.added_product_to_shopping_cart, Snackbar.LENGTH_LONG).show();
            }
        });

//        binding.deleteItemFromWishListButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Log.d("PDF","masuk on click delete item");
//                Snackbar.make(v, R.string.delete_product_from_wishlist, Snackbar.LENGTH_LONG).show();
////                binding.deleteItemFromWishListButton.hide();
////                binding.fab.show();
//            }
//        });

        setHasOptionsMenu(true);
        return binding.getRoot();
    }

    private void setupViewModel(String plantId) {
        ProductDetailViewModelFactory factory = InjectorUtils
                .provideproductDetailViewModelFactory(getActivity(), plantId);
        mViewModel = ViewModelProviders.of(this, factory).get(ProductDetailViewModel.class);
        mViewModel.product.observe(getViewLifecycleOwner(), new Observer<Product>() {
            @Override
            public void onChanged(@Nullable Product product) {
                Log.e(TAG, "onChanged: "+ product.getName() );
                shareText = product == null ? "" :
                        String.format(getString(R.string.share_text_product), product.getName());
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_plant_detail, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }
}
