package com.zeeroapps.sunflower.ui;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.zeeroapps.sunflower.R;
import com.zeeroapps.sunflower.adapters.ProductAdapter;
import com.zeeroapps.sunflower.data.Product;
import com.zeeroapps.sunflower.utils.InjectorUtils;
import com.zeeroapps.sunflower.view_models.ProductListViewModel;
import com.zeeroapps.sunflower.view_models.ProductListViewModelFactory;

import java.util.ArrayList;
import java.util.List;

public class ProductListFragment extends Fragment {

    private List<Product> plantsList = new ArrayList<>();
    private ProductListViewModel viewModel;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_product_list, container, false);

        ProductListViewModelFactory factory = InjectorUtils.provideproductListViewModelFactory(getActivity());
        viewModel = ViewModelProviders.of(this, factory).get(ProductListViewModel.class);


        ProductAdapter adapter = new ProductAdapter(plantsList);

        RecyclerView recyclerView = view.findViewById(R.id.product_list);
        recyclerView.setAdapter(adapter);
        subscribeUI(adapter);

        setHasOptionsMenu(true);

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_plant_list, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    private void subscribeUI(ProductAdapter adapter) {
        viewModel.getproducts().observe(getViewLifecycleOwner(), new Observer<List<Product>>() {
            @Override
            public void onChanged(@Nullable List<Product> products)
            {
                Log.d("Check datasize",Integer.toString(products.size()));
                adapter.updateItems(products);
            }
        });
    }

}
