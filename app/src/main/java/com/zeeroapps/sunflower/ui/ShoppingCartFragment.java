package com.zeeroapps.sunflower.ui;


import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.zeeroapps.sunflower.R;
import com.zeeroapps.sunflower.adapters.ShoppingCartProductAdapter;
import com.zeeroapps.sunflower.data.ProductAndShoppingCartProducts;
import com.zeeroapps.sunflower.data.ShoppingCartProduct;
import com.zeeroapps.sunflower.utils.InjectorUtils;
import com.zeeroapps.sunflower.view_models.ShoppingCartProductListViewModel;
import com.zeeroapps.sunflower.view_models.ShoppingCartProductListViewModelFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class ShoppingCartFragment extends Fragment {

    List<ProductAndShoppingCartProducts> productingsList = new ArrayList<>();
    RecyclerView mRecyclerView;
    TextView mTextViewEmptyShoppingCart;

    public ShoppingCartFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_shopping_cart_list, container, false);
        ShoppingCartProductAdapter adapter = new ShoppingCartProductAdapter(productingsList);
        adapter.setContext(getContext());


        mTextViewEmptyShoppingCart = view.findViewById(R.id.empty_ShoppingCart);
        mRecyclerView = view.findViewById(R.id.ShoppingCart_list);
        mRecyclerView.setAdapter(adapter);


        Log.d("GF","sebelum subscribeUI");

        subscribeUI(adapter);
        Log.d("GF",adapter.getItemCount()+"");
        return view;
    }

    private void subscribeUI(ShoppingCartProductAdapter adapter) {
        Log.d("GF","viewModel.ShoppingCartProducts is null:");
        ShoppingCartProductListViewModelFactory factory = InjectorUtils.provideShoppingCartProductListViewModelFactory(getContext());
        ShoppingCartProductListViewModel viewModel = ViewModelProviders.of(this, factory).get(ShoppingCartProductListViewModel.class);
        viewModel.ShoppingCartProducts.observe(getViewLifecycleOwner(), new Observer<List<ShoppingCartProduct>>() {
            @Override
            public void onChanged(@Nullable List<ShoppingCartProduct> ShoppingCartProducts) {
                if (ShoppingCartProducts != null && !ShoppingCartProducts.isEmpty()) {
                    mRecyclerView.setVisibility(View.VISIBLE);
                    mTextViewEmptyShoppingCart.setVisibility(View.GONE);
                }else {
                    mRecyclerView.setVisibility(View.GONE);
                    mTextViewEmptyShoppingCart.setVisibility(View.VISIBLE);
                }
            }
        });

        Log.d("GF", "viewModel.ShoppingCartProducts is null:"+(viewModel.ShoppingCartProducts == null));
        Log.d("GF", "viewModel.productAndShoppingCartProducts is null:"+(viewModel.productAndShoppingCartProducts == null));
        Log.d("GF", "viewModel.ShoppingCartProductRepository is null:"+(viewModel.ShoppingCartProductRepository == null));
        adapter.setShoppingCartProductRepository(viewModel.ShoppingCartProductRepository);
        Log.d("GF", "adapter.ShoppingCartProductRepository is null:"+(adapter.getShoppingCartProductRepository() == null));








        viewModel.productAndShoppingCartProducts.observe(getViewLifecycleOwner(), new Observer<List<ProductAndShoppingCartProducts>>() {
            @Override
            public void onChanged(@Nullable List<ProductAndShoppingCartProducts> ShoppingCartProductsList) {
                for (int i = 0;i<ShoppingCartProductsList.size();i++){
                    Log.d("onChanged GF,i1: ", ShoppingCartProductsList.get(i).getProduct().getName());
                    Log.d("onChanged GF,i2:", Integer.toString(ShoppingCartProductsList.get(i).getShoppingCartProducts().size()));
                }

                if (ShoppingCartProductsList != null && !ShoppingCartProductsList.isEmpty()) {
                    adapter.updateList(ShoppingCartProductsList);
                }
            }

        });

        Log.d("GF","Size adapter"+adapter.getItemCount()+"");
        Log.d("GF","wooooooooooyyyyyyyyyy");
        Log.d("GF","Size adapter"+adapter.getItemCount()+"");
        Log.d("GF","Size adapter"+adapter.getItemCount()+"");
        Log.d("GF","Size adapter"+adapter.getItemCount()+"");

        Log.d("GF","wooooooooooyyyyyyyyyy");






    }



}
