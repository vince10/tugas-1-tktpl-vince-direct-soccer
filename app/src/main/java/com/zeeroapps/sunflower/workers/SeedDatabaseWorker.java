package com.zeeroapps.sunflower.workers;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.zeeroapps.sunflower.data.AppDatabase;
import com.zeeroapps.sunflower.data.Product;

import java.io.InputStream;
import java.util.List;

import androidx.work.Worker;
import androidx.work.WorkerParameters;

public class SeedDatabaseWorker extends Worker {


    public SeedDatabaseWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
    }

    @NonNull
    @Override
    public Result doWork() {

        try {
            InputStream inputStream = getApplicationContext().getAssets().open("products.json");
            int size = inputStream.available();
            byte[] buffer = new byte[size];
            inputStream.read(buffer);
            inputStream.close();
            String json = new String(buffer, "UTF-8");
            List<Product> productList = new Gson().fromJson(json, new TypeToken<List<Product>>() {
            }.getType());
            AppDatabase appDatabase = AppDatabase.getInstance(getApplicationContext());
            appDatabase.productDao().insertAll(productList);

            Log.d("SDW","success");

            return Result.success();
        } catch (Exception e) {
            e.printStackTrace();
            return Result.failure();
        }
    }
}
