package com.zeeroapps.sunflower.adapters;

import androidx.databinding.DataBindingUtil;
import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zeeroapps.sunflower.R;
import com.zeeroapps.sunflower.data.ProductAndWishProducts;
import com.zeeroapps.sunflower.data.WishProduct;
import com.zeeroapps.sunflower.data.WishProductRepository;
import com.zeeroapps.sunflower.databinding.ListItemWishProductBinding;
//import com.zeeroapps.sunflower.ui.GardenFragmentDirections;
import com.zeeroapps.sunflower.ui.WishListFragmentDirections;
import com.zeeroapps.sunflower.view_models.ProductAndWishProductsViewModel;

import java.util.List;

public class WishProductAdapter extends RecyclerView.Adapter<WishProductAdapter.ViewHolder> {

    List<ProductAndWishProducts> plantingsList;
    WishProductRepository wishProductRepository;
    Context context;

    public WishProductAdapter(List<ProductAndWishProducts> plantingsList) {
        this.plantingsList = plantingsList;

    }

    public WishProductRepository getWishProductRepository() {
        return wishProductRepository;
    }

    public void setWishProductRepository(WishProductRepository wishProductRepository) {
        this.wishProductRepository = wishProductRepository;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public Context getContext() {
        return context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int position) {
        ListItemWishProductBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()), R.layout.list_item_wish_product, parent, false);

        //binding.deleteItemFromWishListButton.setOnClickListener(createClickListener(this.plantingsList.get(position).getPlant().getProductId()));
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        ProductAndWishProducts product  = plantingsList.get(position);
        //viewHolder.bind(product);
        //
        viewHolder.bind(product,createClickListener(product.getProduct().getProductId()));

    }

    @Override
    public int getItemCount() {
        return plantingsList.size();
    }

    public void updateList(List<ProductAndWishProducts> plantings) {
        WishProductDiffCallback diffCallback = new WishProductDiffCallback(this.plantingsList, plantings);
        DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(diffCallback);

        this.plantingsList.clear();
        this.plantingsList.addAll(plantings);
        diffResult.dispatchUpdatesTo(this);
    }

    private View.OnClickListener createClickListener(String plantId) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                WishListFragmentDirections.ActionWishlistFragmentToProductDetailFragment direction = WishListFragmentDirections.actionWishlistFragmentToProductDetailFragment(plantId,"wishlist");


//
//                PlantListFragmentDirections.ActionPlantListFragmentToPlantDetailFragment direction =
//                        PlantListFragmentDirections.actionPlantListFragmentToPlantDetailFragment(plantId,"product");
                Navigation.findNavController(view).navigate(direction);

            }
        };
    }

    private View.OnClickListener deleteWishItemClickListener(String plantId){
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("GPA","plantID: "+plantId);
                ProductAndWishProducts productAndWishProducts = null;
                Log.d("GPA","sebelum loop");
                for (ProductAndWishProducts obj: plantingsList){
                    if (obj.getProduct().getProductId().equals(plantId)){
                        productAndWishProducts = obj;
                    }
                }
                Log.d("GPA","abis loop");
                Log.d("GPA","size sebelum deletion: "+getItemCount());
                Log.d("GPA","name: "+ productAndWishProducts.getProduct().getName());
                Log.d("GPA","size: "+ productAndWishProducts.getWishProducts().size());
                for(WishProduct obj: productAndWishProducts.getWishProducts()){
                   wishProductRepository.deletewishproduct(obj);
                }





            }
        };
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        private ListItemWishProductBinding binding;

        ViewHolder(@NonNull ListItemWishProductBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            binding.deleteItemFromWishListButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d("GPA","masuk onclicknyabutton");
                    List<WishProduct> wishProductList = plantingsList.get(getAdapterPosition()).getWishProducts();
                    for (WishProduct obj: wishProductList){
                        wishProductRepository.deletewishproduct(obj);
                    }
                    Log.d("GPA","done on click");
                }
            });
        }

        void bind(ProductAndWishProducts productAndWishProducts, View.OnClickListener clickListener2) {
            binding.setClickListener(clickListener2);
            //binding.deleteItemFromWishListButton.setOnClickListener(clickListener2);

            binding.setViewModel(new ProductAndWishProductsViewModel(
                    binding.getRoot().getContext(), productAndWishProducts));
            binding.executePendingBindings();



        }


//        public void bind(PlantAndWishProducts product, View.OnClickListener clickListenerDeleteButton) {
//            binding.deleteItemFromWishListButton.setOnClickListener(clickListenerDeleteButton);
//        }
    }
}

