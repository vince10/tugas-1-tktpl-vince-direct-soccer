package com.zeeroapps.sunflower.adapters;

import androidx.recyclerview.widget.DiffUtil;

import com.zeeroapps.sunflower.data.ProductAndShoppingCartProducts;

import java.util.List;

public class ShoppingCartProductDiffCallback extends DiffUtil.Callback {

    private List<ProductAndShoppingCartProducts> oldList;
    private List<ProductAndShoppingCartProducts> newList;

    public ShoppingCartProductDiffCallback(List<ProductAndShoppingCartProducts> oldList, List<ProductAndShoppingCartProducts> newList) {
        this.oldList = oldList;
        this.newList = newList;
    }


    @Override
    public int getOldListSize() {
        return this.oldList.size();
    }

    @Override
    public int getNewListSize() {
        return this.newList.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        ProductAndShoppingCartProducts oldPlant = oldList.get(oldItemPosition);
        ProductAndShoppingCartProducts newPlant = newList.get(newItemPosition);
        return oldPlant.getProduct().getProductId() == newPlant.getProduct().getProductId();
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        ProductAndShoppingCartProducts oldPlant = oldList.get(oldItemPosition);
        ProductAndShoppingCartProducts newPlant = newList.get(newItemPosition);
        return oldPlant.equals(newPlant);
    }

}
