package com.zeeroapps.sunflower.adapters;

import androidx.databinding.DataBindingUtil;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.zeeroapps.sunflower.R;
import com.zeeroapps.sunflower.data.Product;
import com.zeeroapps.sunflower.databinding.ListItemProductBinding;
import com.zeeroapps.sunflower.ui.ProductListFragmentDirections;

import java.util.List;

import androidx.navigation.Navigation;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ViewHolder>{
    private List<Product> productList;
    private LayoutInflater layoutInflater;

    public ProductAdapter(List<Product> products) {
        productList = products;
    }



    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.getContext());
        }
        ListItemProductBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.list_item_product, parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Product product = productList.get(position);

        holder.bind(product, createClickListener(product.getProductId()));
    }

    private View.OnClickListener createClickListener(String ProductId) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                ProductListFragmentDirections.ActionProductListFragmentToProductDetailFragment direction =
                        ProductListFragmentDirections.actionProductListFragmentToProductDetailFragment(ProductId,"product");
                Navigation.findNavController(view).navigate(direction);

            }
        };
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    public void updateItems(List<Product> products) {

        final ProductDiffCallback diffCallback = new ProductDiffCallback(this.productList, products);
        final DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(diffCallback);

        productList.clear();
        productList.addAll(products);
        diffResult.dispatchUpdatesTo(this);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ListItemProductBinding binding;
        ViewHolder(@NonNull ListItemProductBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void bind(Product product, View.OnClickListener clickListener) {
            binding.setClickListener(clickListener);
            binding.setProduct(product);
            binding.executePendingBindings();

        }
    }
}
