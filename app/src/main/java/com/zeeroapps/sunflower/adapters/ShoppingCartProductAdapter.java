package com.zeeroapps.sunflower.adapters;

import androidx.databinding.DataBindingUtil;
import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zeeroapps.sunflower.R;
import com.zeeroapps.sunflower.data.ProductAndShoppingCartProducts;
import com.zeeroapps.sunflower.data.ShoppingCartProduct;
import com.zeeroapps.sunflower.data.ShoppingCartProductRepository;
import com.zeeroapps.sunflower.databinding.ListItemShoppingCartProductBinding;
//import com.zeeroapps.sunflower.ui.GardenFragmentDirections;
import com.zeeroapps.sunflower.ui.ShoppingCartFragmentDirections;
import com.zeeroapps.sunflower.view_models.ProductAndShoppingCartProductsViewModel;

import java.util.List;

public class ShoppingCartProductAdapter extends RecyclerView.Adapter<ShoppingCartProductAdapter.ViewHolder> {

    List<ProductAndShoppingCartProducts> plantingsList;
    ShoppingCartProductRepository ShoppingCartProductRepository;
    Context context;

    public ShoppingCartProductAdapter(List<ProductAndShoppingCartProducts> plantingsList) {
        this.plantingsList = plantingsList;

    }

    public ShoppingCartProductRepository getShoppingCartProductRepository() {
        return ShoppingCartProductRepository;
    }

    public void setShoppingCartProductRepository(ShoppingCartProductRepository ShoppingCartProductRepository) {
        this.ShoppingCartProductRepository = ShoppingCartProductRepository;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public Context getContext() {
        return context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int position) {
        ListItemShoppingCartProductBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()), R.layout.list_item_shopping_cart_product, parent, false);

        //binding.deleteItemFromWishListButton.setOnClickListener(createClickListener(this.plantingsList.get(position).getPlant().getProductId()));
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        ProductAndShoppingCartProducts product  = plantingsList.get(position);
        //viewHolder.bind(product);
        //
        viewHolder.bind(product,createClickListener(product.getProduct().getProductId()));

    }

    @Override
    public int getItemCount() {
        return plantingsList.size();
    }

    public void updateList(List<ProductAndShoppingCartProducts> plantings) {
        ShoppingCartProductDiffCallback diffCallback = new ShoppingCartProductDiffCallback(this.plantingsList, plantings);
        DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(diffCallback);

        this.plantingsList.clear();
        this.plantingsList.addAll(plantings);
        diffResult.dispatchUpdatesTo(this);
    }

    private View.OnClickListener createClickListener(String plantId) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ShoppingCartFragmentDirections.ActionShoppingCartFragmentToProductDetailFragment direction = ShoppingCartFragmentDirections.actionShoppingCartFragmentToProductDetailFragment(plantId,"shopping cart");
//
//                PlantListFragmentDirections.ActionPlantListFragmentToPlantDetailFragment direction =
//                        PlantListFragmentDirections.actionPlantListFragmentToPlantDetailFragment(plantId,"product");
                Navigation.findNavController(view).navigate(direction);

            }
        };
    }

    private View.OnClickListener deleteWishItemClickListener(String plantId){
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("GPA","plantID: "+plantId);
                ProductAndShoppingCartProducts productAndShoppingCartProducts = null;
                Log.d("GPA","sebelum loop");
                for (ProductAndShoppingCartProducts obj: plantingsList){
                    if (obj.getProduct().getProductId().equals(plantId)){
                        productAndShoppingCartProducts = obj;
                    }
                }
                Log.d("GPA","abis loop");
                Log.d("GPA","size sebelum deletion: "+getItemCount());
                Log.d("GPA","name: "+ productAndShoppingCartProducts.getProduct().getName());
                Log.d("GPA","size: "+ productAndShoppingCartProducts.getShoppingCartProducts().size());
                for(ShoppingCartProduct obj: productAndShoppingCartProducts.getShoppingCartProducts()){
                    ShoppingCartProductRepository.deleteShoppingCartProduct(obj);
                }





            }
        };
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        private ListItemShoppingCartProductBinding binding;

        ViewHolder(@NonNull ListItemShoppingCartProductBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            binding.deleteItemFromShoppingCartButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d("GPA","masuk onclicknyabutton");
                    List<ShoppingCartProduct> ShoppingCartProductList = plantingsList.get(getAdapterPosition()).getShoppingCartProducts();
                    for (ShoppingCartProduct obj: ShoppingCartProductList){
                        ShoppingCartProductRepository.deleteShoppingCartProduct(obj);
                    }
                    Log.d("GPA","done on click");
                }
            });
        }

        void bind(ProductAndShoppingCartProducts productAndShoppingCartProducts, View.OnClickListener clickListener2) {
            binding.setClickListener(clickListener2);
            //binding.deleteItemFromWishListButton.setOnClickListener(clickListener2);

            binding.setViewModel(new ProductAndShoppingCartProductsViewModel(
                    binding.getRoot().getContext(), productAndShoppingCartProducts));
            binding.executePendingBindings();



        }


//        public void bind(PlantAndShoppingCartProducts product, View.OnClickListener clickListenerDeleteButton) {
//            binding.deleteItemFromWishListButton.setOnClickListener(clickListenerDeleteButton);
//        }
    }
}

