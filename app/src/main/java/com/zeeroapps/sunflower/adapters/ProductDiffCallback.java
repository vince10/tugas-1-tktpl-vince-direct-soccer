package com.zeeroapps.sunflower.adapters;

import androidx.recyclerview.widget.DiffUtil;

import com.zeeroapps.sunflower.data.Product;

import java.util.List;

public class ProductDiffCallback extends DiffUtil.Callback {

    private List<Product> oldList;
    private List<Product> newList;

    public ProductDiffCallback(List<Product> oldList, List<Product> newList) {
        this.oldList = oldList;
        this.newList = newList;
    }


    @Override
    public int getOldListSize() {
        return this.oldList.size();
    }

    @Override
    public int getNewListSize() {
        return this.newList.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        Product oldProduct = oldList.get(oldItemPosition);
        Product newProduct = newList.get(newItemPosition);
        return oldProduct.getProductId() == newProduct.getProductId();
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        Product oldProduct = oldList.get(oldItemPosition);
        Product newProduct = newList.get(newItemPosition);
        return oldProduct.equals(newProduct);
    }

}
