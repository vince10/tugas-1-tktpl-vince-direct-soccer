package com.zeeroapps.sunflower.adapters;

import androidx.recyclerview.widget.DiffUtil;

import com.zeeroapps.sunflower.data.ProductAndWishProducts;

import java.util.List;

public class WishProductDiffCallback extends DiffUtil.Callback {

    private List<ProductAndWishProducts> oldList;
    private List<ProductAndWishProducts> newList;

    public WishProductDiffCallback(List<ProductAndWishProducts> oldList, List<ProductAndWishProducts> newList) {
        this.oldList = oldList;
        this.newList = newList;
    }


    @Override
    public int getOldListSize() {
        return this.oldList.size();
    }

    @Override
    public int getNewListSize() {
        return this.newList.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        ProductAndWishProducts oldPlant = oldList.get(oldItemPosition);
        ProductAndWishProducts newPlant = newList.get(newItemPosition);
        return oldPlant.getProduct().getProductId() == newPlant.getProduct().getProductId();
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        ProductAndWishProducts oldPlant = oldList.get(oldItemPosition);
        ProductAndWishProducts newPlant = newList.get(newItemPosition);
        return oldPlant.equals(newPlant);
    }

}
